import Vue from 'vue';
import VueRouter from 'vue-router';

import ListExam from './components/Exams/ListExam.vue';
import ListQuestion from './components/Questions/ListQuestion.vue';
import FormQuestion from './components/Questions/QuestionForm.vue';
import ListUser from './components/Users/ListUser.vue';
import Login from './components/Auth/Login.vue';

Vue.use(VueRouter);

const routes = [
    {path: '/exams', name: 'exams', component: ListExam},
    {path: '/questions', name: 'questions', component: ListQuestion},
    {path: '/questions/create', name: 'questioncreate', component: FormQuestion},
    {path: '/users', name: 'users', component: ListUser},
    {path: '/login', name: 'login', component: Login},
];

const router = new VueRouter({
    routes
})

export default router;