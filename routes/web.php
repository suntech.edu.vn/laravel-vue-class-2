<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::namespace('Frontend')->middleware(['auth'])->group(function() {
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('exam/{id}', 'ExamController@show')->name('exam.show');
	Route::post('exam/{id}/done', 'ExamController@done')->name('exam.done');
	Route::get('exam/{id}/report', 'UserController@report')->name('user.report');
});

Auth::routes();
