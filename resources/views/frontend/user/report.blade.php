@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card exam-questions">
                <div class="card-header">
                    Kết quả đề thi: <strong>{{ $exam->name }}</strong>
                    <span style="float: right;">Tổng số câu hỏi: {{ $exam->questions->count() }}</span>
                </div>
                <div class="card-body">
                    <form action="{{ route('exam.done', ['id' => $exam->id]) }}" method="post">
                    @csrf

                    @if(!empty(($questions)))
                        <ul>
                        @foreach ($questions as $key => $question)
                            <li>
                                <b>Câu {{ $key + 1 }}</b>: {{ $question->content }}
                                <ul>
                                    @php
                                        $answerNumber = 'A';    
                                    @endphp
                                    @foreach ($question->answers as $index => $answer)
                                    <li>
                                        <label>
                                            <span class="answer-number {{ $question->answer_id ===  $answer->id ? 'failed' : '' }} {{ $answer->correct ? 'successfully' : '' }}">
                                                {{ chr(ord($answerNumber)+ $index) }}
                                            </span>

                                            {{ $answer->content }}
                                        </label>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                        </ul>
                    @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
