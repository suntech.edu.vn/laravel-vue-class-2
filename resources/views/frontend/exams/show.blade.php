@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card exam-questions">
                <div class="card-header">
                    Bạn đang thi đề: <strong>{{ $exam->name }}</strong>
                    <span style="float: right;">Tổng số câu hỏi: {{ $exam->questions->count() }}</span>
                </div>
                <div class="card-body">
                    <form action="{{ route('exam.done', ['id' => $exam->id]) }}" method="post">
                    @csrf

                    @if(!empty(($questions = $exam->questions)))
                        <ul>
                        @foreach ($questions as $key => $question)
                            <li>
                                <b>Câu {{ $key + 1 }}</b>: {{ $question->content }}
                                <ul>
                                    @php
                                        $answerNumber = 'A';    
                                    @endphp
                                    @foreach ($question->answers as $index => $answer)
                                    <li>
                                        <label>
                                            <span class="answer-number">
                                                {{ chr(ord($answerNumber)+ $index) }}
                                            </span>
                                            
                                            <input type="radio" name="answers[{{ $question->id }}]" value="{{ $answer->id }}" />

                                            {{ $answer->content }}
                                        </label>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                        </ul>
                    @endif
                    <p align="center">
                        <button>Nộp bài</button>
                    </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
