@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Danh sách đề thi</div>
                <div class="card-body">
                    @foreach ($exams->items() as $exam)
                        <div class="card" style="width: 18rem; float: left; margin-right: 35px; margin-bottom: 10px; text-align: center;">
                            <div class="card-body">
                                <h5 class="card-title">{{ $exam->name }}</h5>
                                <p class="card-text">
                                    Thời gian làm bài: 60 phút
                                </p>
                                <p class="card-text">
                                    Số câu hỏi: {{ $exam->questions->count() }}
                                </p>
                                <a href="{{ route('exam.show', ['id' => $exam->id]) }}" class="btn btn-primary">Thi thử</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
