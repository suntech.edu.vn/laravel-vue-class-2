<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class TestHttpRequestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bath:test:http:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $response = Http::withHeaders(['X-First' => 'foo', 'X-Second' => 'bar'])
        //     ->post('http://localhost:8000/api/v1/login', [
        //         'email' => 'phamkykhoi.info@gmail.com',
        //         'password' => 12345678
        //     ]);


        $exams = Http::withHeaders([
            'Authentication' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC92MVwvbG9naW4iLCJpYXQiOjE1OTA2NzMwMzYsImV4cCI6MTU5MDY3NjYzNiwibmJmIjoxNTkwNjczMDM2LCJqdGkiOiIySHJEM3pGWmxJQzNnQk14Iiwic3ViIjo1MDEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.-I4IjQVlryh9JZYE70Y1FF_143pJn7KUrIZKjrmHPuo'
        ])->get('http://localhost:8000/api/v1/exams');

        dd($exams->json());
    }
}
