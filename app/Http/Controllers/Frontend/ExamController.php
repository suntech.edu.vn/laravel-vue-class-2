<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ExamService;
use App\Services\ResultService;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
	protected $examService;

    protected $resultService;

	public function __construct(ExamService $examService, ResultService $resultService)
	{
		$this->examService   = $examService;
        $this->resultService = $resultService;
	}

    public function show($id)
    {
    	$exam = $this->examService->findById($id);
    	return view('frontend.exams.show', ['exam' => $exam]);
    }

    public function done(Request $request, $id)
    {
        try {
            foreach ($request->answers as $questionId => $answerId) {
                $data = [
                    'exam_id'     => $id,
                    'question_id' => $questionId,
                    'answer_id'   => $answerId,
                    'user_id'     => Auth::user()->id,
                ];

                $this->resultService->save($data);
            }

            return redirect(route('exam.show', ['id' => $id]));

        } catch (\Exception $e) {
            dd($e->getMessage());
            report($e);
        }
    }
}
