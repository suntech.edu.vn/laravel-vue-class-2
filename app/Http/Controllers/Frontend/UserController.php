<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ResultService;
use Illuminate\Support\Facades\Auth;
use App\Services\ExamService;

class UserController extends Controller
{
    protected $resultService;

    protected $examService;

    public function __construct(ExamService $examService, ResultService $resultService)
    {
        $this->examService = $examService;
        $this->resultService = $resultService;
    }

    public function report($examId)
    {
        try {
            $exam = $this->examService->findById($examId);
            $questions = $this->resultService->report($examId, Auth::user()->id);

            return view('frontend.user.report', [
                'questions' => $questions,
                'exam'    => $exam,
            ]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            report($e);
        }
    }
}
