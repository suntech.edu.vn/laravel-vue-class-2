<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ExamService;
use App\Models\Exam;

class HomeController extends Controller
{
	protected $examService;

	public function __construct(ExamService $examService)
	{
		$this->examService = $examService;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$exams = $this->examService->getAll(null, 25);

        return view('home', [
        	'exams' => $exams
        ]);
    }
}
