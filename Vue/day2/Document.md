* Data Binding & Two Way Data Binding
- Data Binding dùng cho attribute, style, truyền dữ liệu giữa các Component
	Ex: v-bind:class="{active: true| false}"
		v-bind:style
- Two Way Data Binding dùng cho các input trong Form
	Ex: v-model

* Component
- Mục đích: Tái sử dụng
- Định nghĩa
	
	// Trông component nó giống như instant

	Vue.component('tên_compoent', {
		data() {
			return {
				name: '',
			}
		},
		methods: {

		},
		template: `Code HTML`
	})

* Props
* Created
	- Nó được chạy ngay sau khi instant được tạo
* watcher
	- Lắng nghe một giá trị trong data thay đổi
	-> Cứ thay đổi là được chạy
* Mounted()
	- Chạy khi mọi thứ đã load xong

* sync & $emit

